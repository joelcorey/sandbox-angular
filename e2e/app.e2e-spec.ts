import { SandboxAngularPage } from './app.po';

describe('sandbox-angular App', () => {
  let page: SandboxAngularPage;

  beforeEach(() => {
    page = new SandboxAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
