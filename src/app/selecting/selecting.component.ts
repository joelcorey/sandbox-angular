import { Component, OnInit } from '@angular/core';

@Component({
  /** See the component HTML file for explanation */

  /** Default select method: */
  selector: 'app-selecting',
  /** custom HTML selector */
  /** selector: '[app-selecting]', */
  /** */
  /** selector: 'app-selecting', */

  templateUrl: './selecting.component.html',
  styleUrls: ['./selecting.component.css']
})
export class SelectingComponent implements OnInit {

  constructor() {  }

  ngOnInit() {
  }

}
