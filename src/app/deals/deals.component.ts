import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.css']
})
export class DealsComponent implements OnInit {
  allowDeals = false;
  arrayDeals = [];
  countDeals = 0;
  constructor() { }

  ngOnInit() {
  }

  onDeals() {
    this.countDeals += 1;
    this.arrayDeals.push(this.countDeals);

    if (this.allowDeals) {
      this.allowDeals = false;
    } else {
      this.allowDeals = true;
    }

  }

}
